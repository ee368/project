clear
echo "Setup Shared Memory.  Instantiate DRAM with addresses and values of 0-9"
echo ""
./setupMemory

echo "Run first instance of GPU"
echo ""
./scalarMult

echo "Run second instance of GPU"
echo ""
./scalarMult

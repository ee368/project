//
//  dram.cpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#include "dram.hpp"
#include <map>
void DRAM::setupDRAMSharedMemory() {

	// Get memory segment
	managed_shared_memory dram_segment(open_or_create, "GPU_SharedMemory", 65536);

	// Allocate segment to shared memory map
	DRAM_ShmemAllocator alloc_inst(dram_segment.get_segment_manager());
	dram_segment.construct<DRAM_SHMMap>("GPU_DRAMAddressMap")(std::less<int>(), alloc_inst);


}

DRAM::DRAM() {

	// Get memory segment
	dram_segment = new managed_shared_memory(open_or_create, "GPU_SharedMemory", 65536);

	// Get shared address map
	sharedAddressMap = dram_segment->find<DRAM_SHMMap>("GPU_DRAMAddressMap").first;

}


void DRAM::writeValue(const int addr, int val) {
    
    //Create iterator for hash table
    DRAM_SHMMap :: iterator iter;

    //Check if address is in has table
    if (hasAddr(addr)) {
        
        //Get iterator
        iter = sharedAddressMap->find(addr);

        //Modify value at that key
        iter->second = val;
        
    } else {

        //Insert address and value into hash table if value doesn't exist
        sharedAddressMap->insert(std::pair<const int, int>(addr, val));
        
    }
    
}

bool DRAM::hasAddr(const int addr) {
    
    //Create iterator
    //std::map<int, int>::iterator itr;
    DRAM_SHMMap :: iterator iter;

    // Initialize value for condition
    bool addressFound = false;
    
    //Loop through map
    for (iter = sharedAddressMap->find(addr); iter != sharedAddressMap->end(); ++iter) {
        
        //If found, set value to true and break from loop
        addressFound = true;
        break;
        
    }
    
    //Return found status
    return addressFound;
    
}

int DRAM::readValue(const int addr) {
    
    // Initialize value,  return -1 if not found
    int value = -1;
    
    // Check if address in hash table
    if (hasAddr(addr)) {
        
        // Set return value to one found in hash table
        value = (sharedAddressMap->find(addr))->second;
        
    }
    
    // Return value
    return value;
    
}

void DRAM::prettyPrintKeyValuePair(const int addr) {
    
    //Get value
    int val = readValue(addr);
    
    //Print
    std::cout << "Key: " << addr << " Val: " << val << std::endl;
    
}

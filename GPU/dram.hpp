//
//  dram.hpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#ifndef dram_hpp
#define dram_hpp

#include <stdio.h>
#include <iostream>
#include <iterator>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <functional>
#include <utility>

//Namespace for boost sharedmemory library
using namespace boost::interprocess;

class DRAM {

    public:

		// SharedMemory Typedefs
		typedef int    DRAM_KeyType;
		typedef int  DRAM_MappedType;
		typedef std::pair<const int, int> DRAM_ValueType;
		typedef allocator<DRAM_ValueType, managed_shared_memory::segment_manager> DRAM_ShmemAllocator;
		typedef map<DRAM_KeyType, DRAM_MappedType, std::less<DRAM_KeyType>, DRAM_ShmemAllocator> DRAM_SHMMap;

		//Sets up new shared memory
		static void setupDRAMSharedMemory();

		// Constructor
		DRAM();

        /*
         *
         * Return type: Void
         *
         * Writes value to "DRAM"
         *
         * If in address Hash Table, changes that value
         *
         * If not, inserts to address Hash Table
         *
         *
         */
        void writeValue(const int addr, int val);
    
        /*
         *
         * Return Type: Int
         *
         * If value exists, returns value from address Hash Table
         *
         */
        int readValue(const int addr);
    
        /*
         *
         * Return Type: Void
         *
         * Pretty Prints key value pair in hash table
         *
         */
        void prettyPrintKeyValuePair(const int addr);
    
    private:

        // Shared memory segment
        managed_shared_memory *dram_segment;

        /*
         *
         * STL Map Data Type
         *
         * Key for pair is address
         * Value is value in memory
         *
         * Should be 1:1 with shared memory
         */
        DRAM_SHMMap * sharedAddressMap;

        /*
         *
         * Return Type: Bool
         *
         * Checks if there is an iterator for key value in address Hash Map
         *
         */
        bool hasAddr(const int addr);
    
};



#endif /* dram_hpp */

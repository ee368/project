//
//  top_level.cpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#include "top_level.hpp"

void testCases();
void gpuDemo();

int MultiplyByTwoOperation(int val) {
    
    return val*2;
    
}

int main() {

    gpuDemo();
    
    
    return 0;
}

void testCases() {
    MemoryManager memoryManager = MemoryManager();
    
    for (int i = 0; i < 10; i++) {
        
        memoryManager.dram.writeValue(i, i);
        
    }
    
    std::cout << "******* DRAM Contents *******" << std::endl << std::endl;
    
    for (int i = 0; i < 10; i++) {
        
        memoryManager.dram.prettyPrintKeyValuePair(i);
        
    }
    
    std::cout << std::endl << std::endl << "******* Request Address 5 From L3 Cache *******" << std::endl << std::endl;
    
    int val = memoryManager.readValue(5);
    std::cout << "******* Return Value: " << val << " *******" << std::endl << std::endl;
    
    
    std::cout << std::endl  << "******* Request Address 5 From L3 Cache *******" << std::endl << std::endl;
    val = memoryManager.readValue(5);
    
    std::cout << "******* Cache Value *******" << std::endl;
    memoryManager.cache.prettyPrintKeyValuePair(5);
    
    std::cout << std::endl << std::endl << "******* Update L3 Address 5 *******" << std::endl;
    memoryManager.writeValue(5, 10);
    memoryManager.cache.prettyPrintKeyValuePair(5);
    
    
    std::cout << std::endl << std::endl << "******* Get Undefined Value *******" << std::endl;
    val = memoryManager.readValue(12);
    
    std::cout << std::endl << std::endl << "******* Write Value to Address 12 of L3 Cache *******" << std::endl;
    memoryManager.writeValue(12, 10);
    
    
    val = memoryManager.readValue(12);
    
    memoryManager.cache.prettyPrintKeyValuePair(12);
    
    std::cout << std::endl << std::endl;
}

void gpuDemo() {
    
	MemoryManager::setupSharedMemory();

    MemoryManager manager = MemoryManager();
    
    //Create addresses
    int addr[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int addrSize = 10;
    
    // Put values into DRAM
    for (int i = 0; i < addrSize; i++) {

        manager.dram.writeValue(i, i);

    }
    
    // Print values in DRAM

//    std::cout << "------- All items in DRAM -------" << std::endl << std::endl;
//    for (int i = 0; i < addrSize; i++) {
//
//        manager.dram.prettyPrintKeyValuePair(i);
//
//    }
//    std::cout << std::endl << std::endl;
//
//    // Perform operation
//    std::cout << "------- Perform Mult By Two Operation -------" << std::endl << std::endl;
//    performOperation(addr, addrSize, &MultiplyByTwoOperation, &manager);
//
//    std::cout << std::endl << std::endl;
//
//    std::cout << "------- All items in L3 Cache -------" << std::endl << std::endl;
//    for (int i = 0; i < addrSize; i++) {
//
//        manager.cache.prettyPrintKeyValuePair(i);
//
//    }
//
//    std::cout << std::endl << std::endl;
//
//    // Perform operation
//    std::cout << "------- Perform Mult By Two Operation -------" << std::endl << std::endl;
//    performOperation(addr, addrSize, &MultiplyByTwoOperation, &manager);
//
//    std::cout << std::endl << std::endl;
//
//    std::cout << "------- All items in L3 Cache -------" << std::endl << std::endl;
//    for (int i = 0; i < addrSize; i++) {
//
//        manager.cache.prettyPrintKeyValuePair(i);
//
//    }
    
}

//
//  memory_manager.hpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#ifndef memory_manager_hpp
#define memory_manager_hpp

#include <stdio.h>

#include "dram.hpp"
#include "cache.hpp"

class MemoryManager {
    
    
    
    public:
    
	//Sets up shared memory
	static void setupSharedMemory();

    MemoryManager() {
        
        cache = L3Cache();
        dram = DRAM();
        
    }
    
    // Cache Object
    L3Cache cache;
    
    // DRAM Object
    DRAM dram;
    
    // Write to cache
    void writeValue(int addr, int val);
    
    // Read value
    int readValue(int addr);
    
    private:
    
};

#endif /* memory_manager_hpp */

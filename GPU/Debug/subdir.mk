################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../cache.cpp \
../dram.cpp \
../gpu.cpp \
../memory_manager.cpp \
../top_level.cpp 

OBJS += \
./cache.o \
./dram.o \
./gpu.o \
./memory_manager.o \
./top_level.o 

CPP_DEPS += \
./cache.d \
./dram.d \
./gpu.d \
./memory_manager.d \
./top_level.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



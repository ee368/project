//
//  gpu.cpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#include "gpu.hpp"

void performOperation(int addrs[], int addrSize, Operation opr, MemoryManager *manager) {
    
    // Loop through addresses
    for (int i = 0; i < addrSize; i++) {
        
        // Save the address
        int addr = addrs[i];
        
        // Read value at address (if not in cache, grabs from DRAM)
        int val = manager->readValue(addr);
        
        //Writes value of operation in cache
        manager->writeValue(addr, opr(val));
        
    }
    
}

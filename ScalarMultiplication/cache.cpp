//
//  cache.cpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#include "cache.hpp"
#include "memory_manager.hpp"

void L3Cache::setupL3SharedMemory() {

	// Get memory segment
	managed_shared_memory l3_segment(open_or_create, "GPU_SharedMemory", 65536);

	// Allocate segment to shared memory map
	L3_ShmemAllocator alloc_inst(l3_segment.get_segment_manager());
	l3_segment.construct<L3_SHMMap>("GPU_L3AddressMap")(std::less<int>(), alloc_inst);

}

L3Cache::L3Cache() {

	// Get memory segment
	l3_segment = new managed_shared_memory(open_or_create, "GPU_SharedMemory", 65536);

	// Get shared address map
	sharedAddressMap = l3_segment->find<L3_SHMMap>("GPU_L3AddressMap").first;

}

void L3Cache::writeValue(int addr, int val) {
    
    //Create iterator for hash table
	L3_SHMMap :: iterator iter;
    
    //Check if address is in has table
    if (hasAddr(addr)) {
        
        //Get iterator
        iter = sharedAddressMap->find(addr);
        
        //Modify value at that key
        iter->second = val;
        
    } else {
        
        //Insert address and value into hash table if value doesn't exist
        sharedAddressMap->insert(std::pair<int, int>(addr, val));
        
    }
    
}

bool L3Cache::hasAddr(int addr) {
    
    //Create iterator
	L3_SHMMap :: iterator iter;
    
    // Initialize value for condition
    bool addressFound = false;
    
    //Loop through map
    for (iter = sharedAddressMap->find(addr); iter != sharedAddressMap->end(); ++iter) {
        
        //If found, set value to true and break from loop
        addressFound = true;
        break;
        
    }
    
    //Return found status
    return addressFound;
    
}

int L3Cache::readValue(int addr) {
    
    // Initialize value,  return -1 if not found
    int value = -1;
    
    // Check if address in hash table
    if (hasAddr(addr)) {
        
        // Set return value to one found in hash table
        value = sharedAddressMap->find(addr)->second;
        
    } 
    
    // Return value
    return value;
    
}

void L3Cache::prettyPrintKeyValuePair(int addr) {
    
    //Get value
    int val = readValue(addr);
    
    //Print
    std::cout << "Key: " << addr << " Val: " << val << std::endl;
    
}

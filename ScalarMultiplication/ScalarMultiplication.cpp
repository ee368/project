#include <stdio.h>
#include <iostream>
#include "gpu.hpp"

int MultiplyByTwoOperation(int val) {

    return val*2;

}

int main(int argc, char ** argv) {

	//Create memory manager
	MemoryManager manager;

	//Create array for addresses
	int addr[10] = {0};

	//Loop through arguments and place them into address array
	for (int i = 0; i < 10; i++) {

		addr[i] = i;

	}

	std::cout << "------- All items in L3 Cache Pre-Operation -------" << std::endl << std::endl;
	for (int i = 0; i < 10; i++) {

		manager.cache.prettyPrintKeyValuePair(i);

	}

	std::cout << std::endl << std::endl;

	// Perform operation
	std::cout << "------- Perform Mult By Two Operation -------" << std::endl << std::endl;
	performOperation(addr, 10, &MultiplyByTwoOperation, &manager);

	std::cout << std::endl << std::endl;

	std::cout << "------- All items in L3 Cache Post-Operation -------" << std::endl << std::endl;
	for (int i = 0; i < 10; i++) {

		manager.cache.prettyPrintKeyValuePair(i);

	}

	std::cout << std::endl << std::endl;

	return 0;

}

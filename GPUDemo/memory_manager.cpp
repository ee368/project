//
//  memory_manager.cpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#include "memory_manager.hpp"

void MemoryManager::setupSharedMemory() {

	// Remove previous instance of shared memory
	shared_memory_object::remove("GPU_SharedMemory");

	// Set up memory for each form of memory
	DRAM::setupDRAMSharedMemory();
	L3Cache::setupL3SharedMemory();

}

void MemoryManager::writeValue(int addr, int val) {
    
    //Write to L3
    cache.writeValue(addr, val);
    
}

int MemoryManager::readValue(int addr) {
    
    //Check if cache has value
    if (cache.hasAddr(addr)) {
        
        std::cout << "******* L3 Cache Hit: Addr " << addr << " *******" << std::endl;
        
        //read cache
        return cache.readValue(addr);
        
    } else {
        
        std::cout << "******* L3 Cache Miss: Addr " << addr << " *******" << std::endl;
        
        int val = dram.readValue(addr);
        
        cache.writeValue(addr, val);
        
        //return dram val
        return val;
        
    }
    
}

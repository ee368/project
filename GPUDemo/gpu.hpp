//
//  gpu.hpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#ifndef gpu_hpp
#define gpu_hpp

#include <stdio.h>
#include "cache.hpp"
#include "dram.hpp"
#include "memory_manager.hpp"

// Type definition of an operation callback
typedef int (*Operation)(int);

// Method takes a list of addresses and their size along with a callback of the operation to perform
void performOperation(int addrs[], int addrSize, Operation opr, MemoryManager * manager);

#endif /* gpu_hpp */

/*
 * GPUDemo.cpp
 *
 *  Created on: Apr 17, 2019
 *      Author: evan
 */


#include "gpu.hpp"
#include <stdio.h>
#include <iostream>
#include "unistd.h"

int main(int argc, char** argv) {

	//Create memory instance
	MemoryManager::setupSharedMemory();


	//Create memory manager
	MemoryManager manager;

	//Initialize DRAM with 0-9
	for (int i = 0; i < 10; i++) {

		manager.dram.writeValue(i,i);


	}

	return 0;

}

//
//  cache.hpp
//  GPU
//
//  Created by Evan Stoddard on 4/11/19.
//

#ifndef cache_hpp
#define cache_hpp

#include <stdio.h>
#include <iostream>
#include <iterator>
#include <map>
#include <stdio.h>
#include <iostream>
#include <iterator>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/map.hpp>
#include <boost/interprocess/allocators/allocator.hpp>
#include <functional>
#include <utility>

//Namespace for boost sharedmemory library
using namespace boost::interprocess;

class L3Cache {

// SharedMemory Typedefs
typedef int    L3_KeyType;
typedef int  L3_MappedType;
typedef std::pair<const int, int> L3_ValueType;
typedef allocator<L3_ValueType, managed_shared_memory::segment_manager> L3_ShmemAllocator;
typedef map<L3_KeyType, L3_MappedType, std::less<L3_KeyType>, L3_ShmemAllocator> L3_SHMMap;


public:

	//Sets up new shared memory
	static void setupL3SharedMemory();

	//Constructor
	L3Cache();

    /*
     *
     * Return type: Void
     *
     * Writes value to "L3 Cache"
     *
     * If in address Hash Table, changes that value
     *
     * If not, inserts to address Hash Table
     *
     *
     */
    void writeValue(int addr, int val);
    
    /*
     *
     * Return Type: Int
     *
     * If value exists, returns value from address Hash Table
     *
     */
    int readValue(int addr);
    
    /*
     *
     * Return Type: Void
     *
     * Pretty Prints key value pair in hash table
     *
     */
    void prettyPrintKeyValuePair(int addr);
    
    /*
     *
     * Return Type: Bool
     *
     * Checks if there is an iterator for key value in address Hash Map
     *
     */
    bool hasAddr(int addr);
    
private:

	// Shared memory segment
	managed_shared_memory * l3_segment;

    /*
     *
     * STL Map Data Type
     *
     * Key for pair is address
     * Value is value in memory
     *
     * Should be 1:1 with shared memory
     */
	L3_SHMMap * sharedAddressMap;
    
    
    
};

#endif /* cache_hpp */
